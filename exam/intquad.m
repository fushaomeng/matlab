function Q=intquad(n)
Z=ones(n);
Q=[Z*-1,Z*exp(1);Z*pi,Z];
end
